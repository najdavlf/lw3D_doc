VERSION=v3.0
TGT=lw3d_${VERSION}

BUILD_DIR=build
TEX_DIR=tex
TEXFIG_DIR=texfig
BIB_DIR=bib

PDFIGLATEX_OPTS += -output-directory=figures/
PDFIGLATEX = TEXINPUTS=$(shell pwd)/$(TEXFIG_DIR):$(shell pwd)/figures/:$(shell pwd)/figures/:$(TEXINPUTS) pdflatex $(PDFIGLATEX_OPTS)

PDFLATEX_OPTS += -output-directory=$(BUILD_DIR)
PDFLATEX = TEXINPUTS=$(shell pwd)/$(TEX_DIR):$(shell pwd)/figures/:$(TEXINPUTS) pdflatex $(PDFLATEX_OPTS)

BIBTEX= BSTINPUTS=$(shell pwd)/$(TEX_DIR):$(BSTINPUTS) BIBINPUTS=$(shell pwd)/$(BIB_DIR):$(BIBINPUTS) bibtex

LINK='https://www.lmd.jussieu.fr/~nvillefranque/data/'

TARFIG=lw3D_figures_${VERSION}.tar
FIGS=figures/
VOLD=
VNEW=

deps=$(TEX_DIR)/includes.tex $(TEX_DIR)/figure_templates.tex

default: $(TGT)
all: $(TGT)
$(TGT): $(BUILD_DIR)/$(TGT).pdf
.PHONY : $(TGT) all clean # not a file name

$(BUILD_DIR): 
	mkdir -p $(BUILD_DIR)

$(TARFIG):
	test -d $(FIGS) || wget ${LINK}/lw3D_figures_${VERSION}.tar

$(FIGS): ${TARFIG}
	test -d $(FIGS) || tar xvf lw3D_figures_${VERSION}.tar

# main manuscript
$(BUILD_DIR)/$(TGT).pdf: $(TEX_DIR)/$(TGT).tex $(deps) $(FIGS) $(BUILD_DIR)
	$(PDFLATEX) $(TGT).tex && $(PDFLATEX) $(TGT).tex

$(BUILD_DIR)/$(TGT).bbl: $(BUILD_DIR) $(BIB_DIR)/sciencebib.bib $(TEX_DIR)/$(TGT).tex
	$(PDFLATEX) $(TGT).tex && cd $(BUILD_DIR) && $(BIBTEX) $(TGT) 

clean:
	rm -f $(BUILD_DIR)/* $(TARFIG)
