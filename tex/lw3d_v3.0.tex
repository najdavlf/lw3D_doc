\documentclass[a4paper, 11pt]{article}
\input{includes}
\input{figure_templates}

\title{Note on SPARTACUS LW 3D effects and more}
\author{N. Villefranque}
\date{\today}

\graphicspath{{figures/}}

\begin{document}
\maketitle

\section{Layout} 
First section is a description of the four pathological cases provided by Robin
and Mark. I ran some Monte Carlo experiments on the four cases, trying to
figure out if the bias in the LW comes from heterogeneity or 3D effects or both
--- apparently both.

Second section is a report on ongoing work we are doing with Sophia.  First we
added a new parameter in SPARTACUS LW, which can be used to increase or
decrease emissivity by cloud sides. Then we tuned it (along with other free
parameters) using the 4 pathological cases. 

\section{ecRad vs. Monte Carlo experiments}

Here, v2.0, v3.0 and v3.1 refer to different versions of the 1D profiles input
to ecRad, that is, different ways of aggregating the 3D LES into 1D profiles.
The main difference is on the cloud mask definition. In v2.0, the threshold was
quite large, in v3.0 it is 0 everywhere and in 3.1 it is adaptative
$$twc\_thresh=\max(\min(0.001,mean(twc(:))/10),1e-7);$$ where $twc$ is a
layer-wise total water content (ice+liquid), units $g m^{-3}$.

So, one series of plots per case, atlantic 13, 14, 7 and pacific 19.  For each
series, the first plots show cloud profiles, with FSD and, overlap and cloud
size diagnosed in the LES which will be used in ecRad runs; then clear sky
radiation; then 1D radiation in homogeneized clouds; then 1D in heterogeneous
clouds; then 3D in homogeneous and 3D in heterogeneous. In heterogeneous cases,
there is one line for a "manually tuned" ecRad run where FSD *= 1.65 and if 3D,
Cs *= .5.

\case{atlantic13}
\LES{atlantic13}
\clearsky{atlantic13}
\fluxes{1D}{homo}{atlantic13}
\fluxes{1D}{hete}{atlantic13}
\fluxes{3D}{homo}{atlantic13}
\fluxes{3D}{hete}{atlantic13}

\case{atlantic14}
\LES{atlantic14}
\clearsky{atlantic14}
\fluxes{1D}{homo}{atlantic14}
\fluxes{1D}{hete}{atlantic14}
\fluxes{3D}{homo}{atlantic14}
\fluxes{3D}{hete}{atlantic14}

\case{atlantic7}
\LES{atlantic7}
\clearsky{atlantic7}
\fluxes{1D}{homo}{atlantic7}
\fluxes{1D}{hete}{atlantic7}
\fluxes{3D}{homo}{atlantic7}
\fluxes{3D}{hete}{atlantic7}

\case{pacific19}
\LES{pacific19}
\clearsky{pacific19}
\fluxes{1D}{homo}{pacific19}
\fluxes{1D}{hete}{pacific19}
\fluxes{3D}{homo}{pacific19}
\fluxes{3D}{hete}{pacific19}

\section{Introducing a correction parameter in ecRad and tuning}

Starting from commit 210d7911380f.
We declared and read from namelist a new parameter called \texttt{tune\_side\_emissivity\_factor}.
Here are the lines we added to \texttt{radiation/radiation\_spartacus\_lw.F90}:

\begin{verbatim}
 if (config%do_lw_side_emissivity_tuning) then
   side_emiss(1:ng3D) = side_emiss(1:ng3D)*config%tune_side_emissivity_factor
 end if
\end{verbatim}

(I put the diff file in \texttt{etc/} repo for memory.)

The hypothesis is that the process responsible for the overestimate of 3D
effects in the LW by SPARTACUS is that in 3D fields, photons emitted by the
cloud and exiting the cloud are absorbed in clear sky very close to cloud edge
because there is a lot of vapour there (more than the average clear sky
concentration that is the one used to compute layer wise clear sky optical
depth). So LW emission does not travel far away from the clouds in 3D and also
this part of the layer that is denser in vapour emits more so the net exchange
between clear sky and cloud is smaller than what we diagnose in SPARTACUS;
because the optical depth contrast is smoother than what the 3-region model
does. It's not true for SW because vapour has a small effect and the contrast
in condensate water is in deed sharp enough to be represented accurately by the
3-regions model.

So one thing we could do is add another region to the clear sky in the LW to
model this vapour ``shell" around clouds. This would add a new overlap
parameter to tune and overall complexity to the scheme.

Another, simpler and cruder thing we could do and did, is introduce a factor to
reduce net exchange between "thin" cloud and clear sky regions directly. For
now it is an entirely free parameter. We could, in the future, diagnose it from
other variables, e.g. water vapour concentrations, and keep a free parameter in
this formulation so that we can still tune it.

So once we introduced this parameter, we launched a tuning experiment using
htexplo. It is a semi-automatic, iterative tuning tool. First we need to
specify tunable parameters and their exploration ranges:
\begin{verbatim}
RAD_OVH_FAC  0      2       1      linear
RAD_LW_EMISS 0.2    2       1      linear
RAD_FSD      0.1    2.0     0.705  linear
RAD_DZ_OVP   10     2000    187.22 log
RAD_OVP_HET  10     1000    93.5   log
RAD_CS_LOW   50     1000    247.47 log
RAD_CS_MID   100    30000   10000  log
RAD_CS_HIGH  100    30000   10000  log
\end{verbatim}

Then, the metrics to target. We used TOA LW up and surface LW down in the four
pathological cases presented in this document. We did 5 waves with these four
metrics. Then we added two SW metrics : TOA up and surface down for one cumulus
case (ARMCu 8th hour). We did 3 more waves with these two additional metrics.

Here are the best sampled simulations settings
\begin{verbatim}
t_IDs RAD_OVH_FAC RAD_LW_EMISS RAD_FSD RAD_DZ_OVP RAD_OVP_HET RAD_CS_LOW RAD_CS_MID RAD_CS_HIGH
SCM-8-079 0.8164 0.924 0.176 1357.27 253.30 252.32 17250.72 20036.27
SCM-7-004 0.5006 1.162 0.216  843.71  15.90 414.83   902.37  8089.85
SCM-3-059 1.5484 1.098 0.269  823.24  22.36 991.74  5415.79  4591.04
SCM-7-006 0.8597 1.088 0.204  862.73 779.13 534.61   413.32  5574.49
SCM-1-022 0.0805 1.099 0.267  561.27  88.51 525.19   137.73  5060.20
SCM-1-040 1.8630 0.925 0.353 1839.36  23.84  52.73   270.12 14801.82
SCM-8-046 0.1372 1.192 0.108  912.89  48.61 672.42   308.78 22954.94
SCM-6-024 0.1175 1.093 0.175 1023.52 608.85 345.80   166.99  4239.77
SCM-8-022 1.0692 1.242 0.164  802.57 462.29 797.45   380.05 25047.66
SCM-7-051 1.2288 0.789 0.477  906.43  17.86 251.79  7089.04 12686.31
\end{verbatim}

The implausibility matrix gives a representation of the Not Ruled Out Yet
space.
\begin{figure}\centering
  \includegraphics[width=\textwidth]{InputSpace_wave8_light.pdf}
  \caption{Implausibility matrix for 8th wave}
\end{figure}

Values of our parameter larger than one are somehow compensated by larger cloud
scale in low clouds. However low cloud scale might be constrained independently
by SW metrics; here we only had one case so let's see how it evolves when
adding constraints in the SW!

Next step: add SW metrics for the 4 pathological cases (need to run SW htrdr
first). Also, check LW metrics were calculated at the same altitude in ecRad
and htrdr! Otherwise, run htrdr again!!!

\end{document}
